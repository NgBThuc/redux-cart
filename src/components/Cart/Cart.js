import { useSelector } from "react-redux";
import Card from "../UI/Card";
import classes from "./Cart.module.css";
import CartItem from "./CartItem";

const Cart = (props) => {
  const itemsList = useSelector((state) => state.cart.items);

  return (
    <Card className={classes.cart}>
      {itemsList.length > 0 ? (
        <h2>Your Shopping Cart</h2>
      ) : (
        <h2>Your Shopping Cart Is Empty!!!</h2>
      )}
      <ul>
        {itemsList.map((item) => (
          <CartItem
            key={item.id}
            item={{
              id: item.id,
              title: item.title,
              quantity: item.quantity,
              total: item.total,
              price: item.price,
            }}
          />
        ))}
      </ul>
    </Card>
  );
};

export default Cart;
