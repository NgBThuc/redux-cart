import ProductItem from "./ProductItem";
import classes from "./Products.module.css";

const DUMMY_PRODUCTS = [
  {
    id: "3ed7cdba-9f82-40ec-885d-6411618b8c1c",
    title: "Iphone 13 Pro Max",
    price: 999,
    description: "This phone is a blazing fast phone!!!",
  },
  {
    id: "2e290171-4804-46bc-a98f-598cd60c5449",
    title: "Samsung Galaxy S22 Ultra",
    price: 1099,
    description: "This is the most powerful android phone nowadays!!!",
  },
];

const Products = (props) => {
  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        {DUMMY_PRODUCTS.map((product) => (
          <ProductItem
            key={product.id}
            id={product.id}
            title={product.title}
            price={product.price}
            description={product.description}
          />
        ))}
      </ul>
    </section>
  );
};

export default Products;
