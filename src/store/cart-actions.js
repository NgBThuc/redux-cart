import { cartActions } from "./cart-slice";
import { uiActions } from "./ui-slice";

export const takeCartData = () => {
  return async (dispatch) => {
    const fetchCart = async () => {
      const response = await fetch(
        "https://react-http-179d1-default-rtdb.asia-southeast1.firebasedatabase.app/cart.json"
      );

      if (!response.ok) {
        throw new Error("Fetching cart data failed!!!");
      }

      const data = await response.json();

      return data;
    };

    try {
      dispatch(
        uiActions.showNotification({
          status: "PENDING",
          title: "LOADING...",
          message: "Please wait for a minute!",
        })
      );

      const data = await fetchCart();

      dispatch(
        cartActions.replaceCart({
          items: data?.items || [],
          totalQuantity: data?.totalQuantity || 0,
        })
      );

      dispatch(
        uiActions.showNotification({
          status: "SUCCESS",
          title: "SUCCESS",
          message: "Fetching cart successfully!!!",
        })
      );
    } catch (error) {
      console.error(error);
      dispatch(
        uiActions.showNotification({
          status: "ERROR",
          title: "ERROR",
          message: "Fetching cart failed!!!",
        })
      );
    }
  };
};

export const saveCartData = (cart) => {
  return async (dispatch) => {
    if (!cart.isChange) {
      return;
    }

    const uploadCart = async () => {
      const response = await fetch(
        "https://react-http-179d1-default-rtdb.asia-southeast1.firebasedatabase.app/cart.json",
        {
          method: "PUT",
          body: JSON.stringify({
            items: cart.items,
            totalQuantity: cart.totalQuantity,
          }),
        }
      );

      if (!response.ok) {
        throw new Error("Save cart data failed!!!");
      }
    };

    try {
      dispatch(
        uiActions.showNotification({
          status: "PENDING",
          title: "LOADING...",
          message: "Please wait for a minute!",
        })
      );

      await uploadCart();

      dispatch(
        uiActions.showNotification({
          status: "SUCCESS",
          title: "SUCCESS",
          message: "Saving cart successfully!!!",
        })
      );
    } catch (error) {
      dispatch(
        uiActions.showNotification({
          status: "ERROR",
          title: "ERROR",
          message: "Saving cart failed!!!",
        })
      );
    }
  };
};
