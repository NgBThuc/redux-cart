const { createSlice } = require("@reduxjs/toolkit");

const initialUISlice = {
  isShowCart: false,
  notification: {
    status: "",
    title: "",
    message: "",
  },
};

const uiSlice = createSlice({
  name: "ui",
  initialState: initialUISlice,
  reducers: {
    toggleShowCart(state) {
      state.isShowCart = !state.isShowCart;
    },
    showNotification(state, action) {
      let notificationDetail = action.payload;
      state.notification = notificationDetail;
    },
  },
});

export const uiActions = uiSlice.actions;
export default uiSlice;
